// The MIT License (MIT)
//
// Copyright (c) 2017 aerth aerth@riseup.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

func init() {
	token := os.Getenv("BINTRAY_TOKEN")
	if token == "" {
		fmt.Println("Please set BINTRAY_TOKEN variable before using " + os.Args[0])
		os.Exit(1)
	}
}

func main() {
	if len(os.Args) < 4 {
		fmt.Println(os.Args[0] + " user/repo/project/newfile v0.0.0 localfile")
		return
	}
	outname := os.Args[1]
	version := os.Args[2]
	filename := os.Args[3]
	body, err := os.Open(filename)
	if err != nil {
		fmt.Println(err)
		return
	}
	//name := os.Args[1]
	req, err := http.NewRequest("PUT", "https://api.bintray.com/content/"+outname, body)

	if err != nil {
		fmt.Println(err)
		return
	}
	//spl, _ := filepath.Split(name)
	dir := filepath.Dir(outname)

	pkg := strings.Split(dir, "/")[strings.Count(dir, "/")]

	fmt.Println("Package:", pkg)
	req.Header.Set("X-Bintray-Package", pkg)
	req.Header.Set("X-Bintray-Version", version)
	req.SetBasicAuth(strings.Split(outname, "/")[0], os.Getenv("BINTRAY_TOKEN"))

	client := new(http.Client)
	r, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return
	}
	rbody, err := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		fmt.Println(err)
		return
	}

	if string(rbody) == `{"message":"success"}` {
		//publish
	 req, err = http.NewRequest("POST", "https://api.bintray.com/content/"+outname+"/"+version+"/publish", body)
	 if err != nil {
  		fmt.Println(err)
  		return
  	}
		req.Header.Set("X-Bintray-Package", pkg)
		req.Header.Set("X-Bintray-Version", version)
		req.SetBasicAuth(strings.Split(outname, "/")[0], os.Getenv("BINTRAY_TOKEN"))
		fmt.Println(req.URL.Path)
 	r2, err := client.Do(req)
 	if err != nil {
 		fmt.Println(err)
 		return
 	}
 	rbody, err = ioutil.ReadAll(r2.Body)
 	r2.Body.Close()
 	if err != nil {
 		fmt.Println(err)
 		return
 	}
	}
	fmt.Println(string(rbody))
}
